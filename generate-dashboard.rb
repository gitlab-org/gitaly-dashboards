#!/usr/bin/env ruby

require 'yaml'
require 'json'

def row_source
<<-HEREDOC
{
    "collapse": false,
    "height": 272,
    "panels": [],
    "repeat": null,
    "repeatIteration": null,
    "repeatRowId": null,
    "showTitle": true,
    "title": "Dashboard Row",
    "titleSize": "h6"
}
HEREDOC
end

def prom_panel_source
<<-HEREDOC
{
    "aliasColors": {},
    "bars": false,
    "datasource": "Prometheus in Azure",
    "fill": 1,
    "height": "200",
    "id": 1,
    "legend": {
    "avg": false,
    "current": false,
    "max": false,
    "min": false,
    "show": true,
    "total": false,
    "values": false
    },
    "lines": true,
    "linewidth": 1,
    "links": [],
    "nullPointMode": "null",
    "percentage": false,
    "pointradius": 5,
    "points": false,
    "renderer": "flot",
    "seriesOverrides": [],
    "span": 3,
    "stack": false,
    "steppedLine": false,
    "targets": [
    {
        "dsType": "influxdb",
        "expr": "",
        "groupBy": [
        {
            "params": [
            "$__interval"
            ],
            "type": "time"
        },
        {
            "params": [
            "null"
            ],
            "type": "fill"
        }
        ],
        "interval": "",
        "intervalFactor": 2,
        "legendFormat": "{{grpc_code}}",
        "policy": "default",
        "refId": "A",
        "resultFormat": "time_series",
        "select": [
        [
            {
            "params": [
                "value"
            ],
            "type": "field"
            },
            {
            "params": [],
            "type": "mean"
            }
        ]
        ],
        "step": 40,
        "tags": []
    }
    ],
    "thresholds": [],
    "timeFrom": null,
    "timeShift": null,
    "title": "CommitDiff gRPC code rates",
    "tooltip": {
    "shared": true,
    "sort": 0,
    "value_type": "individual"
    },
    "type": "graph",
    "xaxis": {
    "mode": "time",
    "name": null,
    "show": true,
    "values": []
    },
    "yaxes": [
    {
        "format": "short",
        "label": null,
        "logBase": 1,
        "max": null,
        "min": 0,
        "show": true
    },
    {
        "format": "short",
        "label": null,
        "logBase": 1,
        "max": null,
        "min": 0,
        "show": false
    }
    ]
}
HEREDOC
end

def latency_panel_source
<<-HEREDOC
{
    "aliasColors": {},
    "bars": false,
    "dashLength": 10,
    "dashes": false,
    "datasource": "Prometheus in Azure",
    "decimals": 2,
    "fill": 0,
    "id": 63,
    "legend": {
    "avg": false,
    "current": false,
    "max": true,
    "min": true,
    "show": true,
    "total": false,
    "values": true
    },
    "lines": true,
    "linewidth": 2,
    "links": [],
    "nullPointMode": "null",
    "percentage": false,
    "pointradius": 5,
    "points": false,
    "renderer": "flot",
    "seriesOverrides": [],
    "spaceLength": 10,
    "span": 12,
    "stack": false,
    "steppedLine": false,
    "targets": [
    {
        "expr": "",
        "format": "time_series",
        "interval": "",
        "intervalFactor": 2,
        "legendFormat": "p99",
        "refId": "B",
        "step": 10
    },
    {
        "expr": "",
        "format": "time_series",
        "interval": "",
        "intervalFactor": 2,
        "legendFormat": "p99",
        "refId": "B",
        "step": 10
    },
    {
        "expr": "",
        "format": "time_series",
        "interval": "",
        "intervalFactor": 2,
        "legendFormat": "p50",
        "refId": "C",
        "step": 10
    }
    ],
    "thresholds": [],
    "timeFrom": null,
    "timeShift": null,
    "title": "Latencies",
    "tooltip": {
    "shared": true,
    "sort": 2,
    "value_type": "individual"
    },
    "type": "graph",
    "xaxis": {
    "buckets": null,
    "mode": "time",
    "name": null,
    "show": true,
    "values": []
    },
    "yaxes": [
    {
        "format": "s",
        "label": null,
        "logBase": 10,
        "max": null,
        "min": null,
        "show": true
    },
    {
        "format": "short",
        "label": null,
        "logBase": 1,
        "max": null,
        "min": null,
        "show": true
    }
    ]
}
HEREDOC
end

def influx_panel_source
<<-HEREDOC
{
    "aliasColors": {
        "95th Percentile": "#9AC48A",
        "99th Percentile": "#E24D42",
        "Mean": "#6ED0E0"
    },
    "bars": false,
    "datasource": "$database",
    "editable": true,
    "error": false,
    "fill": 0,
    "height": "200",
    "grid": {},
    "id": 3,
    "interval": "1m",
    "legend": {
        "avg": false,
        "current": false,
        "max": false,
        "min": false,
        "show": true,
        "total": false,
        "values": false
    },
    "lines": true,
    "linewidth": 1,
    "links": [],
    "nullPointMode": "null as zero",
    "percentage": false,
    "pointradius": 5,
    "points": false,
    "renderer": "flot",
    "seriesOverrides": [],
    "span": 3,
    "stack": false,
    "steppedLine": false,
    "targets": [
        {
            "alias": "$col",
            "dsType": "influxdb",
            "groupBy": [
                {
                    "params": [
                        "$interval"
                    ],
                    "type": "time"
                },
                {
                    "params": [
                        "null"
                    ],
                    "type": "fill"
                }
            ],
            "policy": "default",
            "query": "",
            "rawQuery": true,
            "refId": "A",
            "resultFormat": "time_series",
            "select": [
                [
                    {
                        "params": [
                            "value"
                        ],
                        "type": "field"
                    },
                    {
                        "params": [],
                        "type": "mean"
                    }
                ]
            ],
            "tags": []
        }
    ],
    "thresholds": [],
    "timeFrom": null,
    "timeShift": null,
    "title": "Transaction Timings",
    "tooltip": {
        "msResolution": true,
        "shared": true,
        "sort": 0,
        "value_type": "cumulative"
    },
    "type": "graph",
    "xaxis": {
        "mode": "time",
        "name": null,
        "show": true,
        "values": []
    },
    "yaxes": [
        {
            "format": "ms",
            "logBase": 1,
            "max": null,
            "min": null,
            "show": true
        },
        {
            "format": "short",
            "logBase": 1,
            "max": null,
            "min": null,
            "show": true
        }
    ]
}
HEREDOC
end

def text_panel_source
<<-HEREDOC
{
    "content": "",
    "id": 1,
    "links": [],
    "mode": "markdown",
    "span": 3,
    "height": "200",
    "title": "",
    "type": "text"
}
HEREDOC
end

def total_calls_by_code_expr(grpc_methods)
    grpc_methods.map { |grpc_method|
        "gitaly:grpc_server_handled_total:rate1m{job=\"$job\",grpc_method=\"#{grpc_method}\"}"
    }.join(' + ')
end

def successful_calls_expr(grpc_methods)
    grpc_methods.map { |grpc_method|
        "gitaly:grpc_server_handled_total:rate1m{job=\"$job\",grpc_method=\"#{grpc_method}\"} - gitaly:grpc_server_handled_total:error_rate1m{job=\"$job\", grpc_method=\"#{grpc_method}\"}"
    }.join(' + ')
end

def failure_calls_expr(grpc_methods)
    grpc_methods.map { |grpc_method|
        "gitaly:grpc_server_handled_total:error_rate1m{job=\"$job\", grpc_method=\"#{grpc_method}\"}"
    }.join(' + ')
end

def latency_expr(grpc_methods, percentile)
    grpc_methods.map { |grpc_method|
        "histogram_quantile(#{percentile}, sum(gitaly:grpc_server_handling_seconds_bucket:rate1m{grpc_method=\"#{grpc_method}\"}) by (le))"
    }.join(' + ')
end

def transaction_timings_query(rails_controllers)
    'SELECT "duration_95th" AS "95th Percentile", "duration_99th" AS "99th Percentile", "duration_mean" AS "Mean" FROM downsampled.rails_transaction_timings_per_action WHERE $timeFilter AND (' +
    rails_controllers.map { |controller |
        "\"action\" = '#{controller}'"
    }.join(' OR ') + ')'
end

def git_timings_query(rails_controllers)
    'SELECT "duration_95th" AS "95th Percentile", "duration_99th" AS "99th Percentile", "duration_mean" AS "Mean" FROM downsampled.rails_git_timings_per_action WHERE $timeFilter AND (' +
    rails_controllers.map { |controller |
        "\"action\" = '#{controller}'"
    }.join(' OR ') + ')'
end

dashboard_file = File.read('gitaly-features.json')
dashboard = JSON.parse(dashboard_file)

features = YAML.load_file('features.yml')
rows = []
dashboard['rows'] = rows

@panel_id = 0

def next_panel_id
    @panel_id += 1
    return @panel_id
end

features.each do |feature|
    grpc_methods = feature['grpc_methods']
    conversation_issue_number = feature['conversation_issue_number']
    runbook_link = feature['runbook_link']
    feature_flag = feature['feature_flag']
    rails_controllers = feature['rails_controllers']

    row = JSON.parse(row_source)
    row['title'] = feature['name']
    rows.push(row)

    panels = row['panels'];

    panel_total_by_code = JSON.parse(prom_panel_source)
    panel_total_by_code['targets'][0]['expr'] = total_calls_by_code_expr(grpc_methods)
    panel_total_by_code['title'] = "Total Rate (per second)"
    panel_total_by_code['id'] = next_panel_id

    panel_successful_calls = JSON.parse(prom_panel_source)
    panel_successful_calls['targets'][0]['expr'] = successful_calls_expr(grpc_methods)
    panel_successful_calls['title'] = "Success Rate (per second)"
    panel_successful_calls['id'] = next_panel_id

    panel_failure_calls = JSON.parse(prom_panel_source)
    panel_failure_calls['targets'][0]['expr'] = failure_calls_expr(grpc_methods)
    panel_failure_calls['title'] = "Failures Rate (per second)"
    panel_failure_calls['id'] = next_panel_id

    panel_text = JSON.parse(text_panel_source)
    panel_text['id'] = next_panel_id
    panel_text['content'] = %Q(
**Conversation**: [\##{conversation_issue_number}](https://gitlab.com/gitlab-org/gitaly/issues/#{conversation_issue_number})

**Runbook**: [link](#{runbook_link})

**Feature Flag**: #{feature_flag ? '`' + feature_flag + '`' : 'none'}
)

    latency_panel = JSON.parse(latency_panel_source)
    latency_panel['targets'][0]['expr'] = latency_expr(grpc_methods, 0.99)
    latency_panel['targets'][1]['expr'] = latency_expr(grpc_methods, 0.95)
    latency_panel['targets'][2]['expr'] = latency_expr(grpc_methods, 0.50)
    latency_panel['title'] = "Latencies"
    latency_panel['id'] = next_panel_id

    panels.push(panel_total_by_code)
    panels.push(panel_successful_calls)
    panels.push(panel_failure_calls)
    panels.push(panel_text)
    panels.push(latency_panel)

    if rails_controllers and rails_controllers.length > 0 then
        panel_rails_timings = JSON.parse(influx_panel_source)
        panel_rails_timings['targets'][0]['query'] = transaction_timings_query(rails_controllers)
        panel_rails_timings['title'] = "Transaction Timings"
        panel_rails_timings['id'] = next_panel_id
        panel_rails_timings['span'] = 3

        panel_git_timings = JSON.parse(influx_panel_source)
        panel_git_timings['targets'][0]['query'] = git_timings_query(rails_controllers)
        panel_git_timings['title'] = "Git Timings"
        panel_git_timings['id'] = next_panel_id
        panel_git_timings['span'] = 3

        panels.push(panel_rails_timings)
        panels.push(panel_git_timings)
    end

end

File.write('gitaly-features.json', JSON.pretty_generate(dashboard))
