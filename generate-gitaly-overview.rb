#!/usr/bin/env ruby

require 'yaml'
require 'json'

def overview_source
<<-HEREDOC
{
  "__inputs": [
    {
      "name": "DS_PROMETHEUS_IN AZURE",
      "label": "Prometheus in Azure",
      "description": "",
      "type": "datasource",
      "pluginId": "prometheus",
      "pluginName": "Prometheus"
    }
  ],
  "__requires": [
    {
      "type": "grafana",
      "id": "grafana",
      "name": "Grafana",
      "version": "4.3.2"
    },
    {
      "type": "datasource",
      "id": "prometheus",
      "name": "Prometheus",
      "version": "1.0.0"
    },
    {
      "type": "panel",
      "id": "singlestat",
      "name": "Singlestat",
      "version": ""
    }
  ],
  "annotations": {
    "list": []
  },
  "editable": true,
  "gnetId": null,
  "graphTooltip": 0,
  "hideControls": false,
  "id": null,
  "links": [],
  "rows": [
    {
      "collapse": false,
      "height": "100px",
      "panels": [],
      "repeat": null,
      "repeatIteration": null,
      "repeatRowId": null,
      "showTitle": false,
      "title": "Overview",
      "titleSize": "h6"
    }
  ],
  "schemaVersion": 14,
  "style": "dark",
  "tags": [],
  "templating": {
    "list": [
      {
        "allValue": null,
        "current": {
          "tags": [],
          "text": "InfoRefsReceivePack",
          "value": "InfoRefsReceivePack"
        },
        "hide": 0,
        "includeAll": false,
        "label": null,
        "multi": false,
        "name": "Method",
        "options": [
          {
            "selected": false,
            "text": "InfoRefsUploadPack",
            "value": "InfoRefsUploadPack"
          },
          {
            "selected": true,
            "text": "InfoRefsReceivePack",
            "value": "InfoRefsReceivePack"
          }
        ],
        "query": "InfoRefsUploadPack,InfoRefsReceivePack",
        "type": "custom"
      }
    ]
  },
  "time": {
    "from": "now-12h",
    "to": "now"
  },
  "timepicker": {
    "refresh_intervals": [
      "5s",
      "10s",
      "30s",
      "1m",
      "5m",
      "15m",
      "30m",
      "1h",
      "2h",
      "1d"
    ],
    "time_options": [
      "5m",
      "15m",
      "1h",
      "6h",
      "12h",
      "24h",
      "2d",
      "7d",
      "30d"
    ]
  },
  "timezone": "utc",
  "title": "Gitaly Features Overview",
  "version": 1
}
HEREDOC
end

def panel_source
<<-HEREDOC
{
    "cacheTimeout": null,
    "colorBackground": true,
    "colorValue": false,
    "colors": [
    "rgba(175, 175, 175, 0.9)",
    "rgba(80, 32, 246, 0)",
    "rgba(16, 98, 13, 0.97)"
    ],
    "datasource": "${DS_PROMETHEUS_IN AZURE}",
    "decimals": 2,
    "format": "none",
    "gauge": {
    "maxValue": 100,
    "minValue": 0,
    "show": false,
    "thresholdLabels": false,
    "thresholdMarkers": true
    },
    "height": "100px",
    "id": 1,
    "interval": null,
    "links": [
        {
            "dashUri": "db/gitaly-feature-status",
            "dashboard": "Gitaly Feature Status",
            "includeVars": false,
            "params": "orgId=1&var-method=InfoRefsUploadPack",
            "title": "Gitaly Feature Status",
            "type": "dashboard"
        }
    ],
    "mappingType": 1,
    "mappingTypes": [
    {
        "name": "value to text",
        "value": 1
    },
    {
        "name": "range to text",
        "value": 2
    }
    ],
    "maxDataPoints": 100,
    "nullPointMode": "connected",
    "nullText": null,
    "postfix": "",
    "postfixFontSize": "50%",
    "prefix": "",
    "prefixFontSize": "50%",
    "rangeMaps": [
    {
        "from": "null",
        "text": "N/A",
        "to": "null"
    }
    ],
    "span": 2,
    "sparkline": {
    "fillColor": "rgba(31, 118, 189, 0.18)",
    "full": false,
    "lineColor": "rgb(31, 120, 193)",
    "show": true
    },
    "tableColumn": "Value",
    "targets": [
    {
        "dsType": "influxdb",
        "expr": "",
        "format": "time_series",
        "groupBy": [
        {
            "params": [
            "$__interval"
            ],
            "type": "time"
        },
        {
            "params": [
            "null"
            ],
            "type": "fill"
        }
        ],
        "intervalFactor": 2,
        "legendFormat": "",
        "orderByTime": "ASC",
        "policy": "default",
        "refId": "A",
        "resultFormat": "time_series",
        "select": [
        [
            {
            "params": [
                "value"
            ],
            "type": "field"
            },
            {
            "params": [],
            "type": "mean"
            }
        ]
        ],
        "step": 600,
        "tags": []
    }
    ],
    "thresholds": "0.01,10000",
    "title": "InfoRefsUploadPack",
    "type": "singlestat",
    "valueFontSize": "80%",
    "valueMaps": [
    {
        "op": "=",
        "text": "N/A",
        "value": "null"
    }
    ],
    "valueName": "avg"
}
HEREDOC
end


def total_calls_by_code_expr(grpc_methods)
    grpc_methods.map { |grpc_method|
        "avg(gitaly:grpc_server_handled_total:rate1m{job=\"gitaly-production\",grpc_method=\"#{grpc_method}\"})"
    }.join(' + ')
end

dashboard = JSON.parse(overview_source)

features = YAML.load_file('features.yml')
panels = []
dashboard['rows'][0]['panels'] = panels

@panel_id = 0

def next_panel_id
    @panel_id += 1
    return @panel_id
end

features.each do |feature|
    grpc_methods = feature['grpc_methods']
    conversation_issue_number = feature['conversation_issue_number']
    runbook_link = feature['runbook_link']
    feature_flag = feature['feature_flag']
    rails_controllers = feature['rails_controllers']

    panel = JSON.parse(panel_source)
    panel['title'] = feature['name']
    panels.push(panel)
    panel['id'] = next_panel_id
    panel['targets'][0]['expr'] = total_calls_by_code_expr(grpc_methods)
    panel['links'][0]['params'] = "orgId=1&var-method=" + grpc_methods[0]
end

File.write('gitaly-features-overview.json', JSON.pretty_generate(dashboard))
